package com.xvidia.cabi.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * This class has setters and getters of HearBeat data
 * @author Ravi@xvidia
 *@since version 1.0
 */
public class LogData {
	
	public static final int TIME_OUT = 20000;
	public static final String MY_PREFERENCES = "CABPREFERENCE";
	public static final String BLUETOOTH_KEY = "BLUETOOTH_KEY";
	public static final String DEVICE_ID = "DEVICE_ID";
	public static final String CAR_REG_NO = "CAR_REG_NO";
	public static final String ASSET_ID = "ASEET_ID";
	public static final String VIDEO_RESTART = "VIDEO_RESTART";
	public static final String STR_NOT_SET = "notSet";
	private static  LogData instance = null;
	 SharedPreferences sharedpreferences;

	public static  LogData getInstance() {
		if (instance == null) {
			instance = new LogData();
		}
		return instance;
	}

	private String getValidatedString(String valStr) {
		if (valStr == null) {
			return STR_NOT_SET;
		} else {
			return valStr.trim();
		}
	}
	public void removeAllPreferenceData(Context ctx){
		SharedPreferences settings = ctx.getSharedPreferences(MY_PREFERENCES,Context.MODE_PRIVATE);
		settings.edit().clear().commit();
	}
	public void removePreferenceData(Context ctx, String preferenceName){
		SharedPreferences settings = ctx.getSharedPreferences(MY_PREFERENCES,Context.MODE_PRIVATE);
		settings.edit().remove(preferenceName).commit();
	}
	private  SharedPreferences getSharedPreference(Context ctx) {
		sharedpreferences = ctx.getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}
	
	    public  String getDeviceId(Context ctx) {
			String retVal = null;
			try {
				retVal = getSharedPreference(ctx).getString(DEVICE_ID, null);
				// retVal = readTextFromFile(FILE_NAME_ASSET);
			} catch (Exception e) {
			}
			return retVal;
		}

		public  boolean setBluetoothKey(String prefVal,Context ctx) {
			boolean retVal = false;
				try {
						Editor editor = getSharedPreference(ctx).edit();
						editor.putString(BLUETOOTH_KEY, prefVal);
						editor.commit();
						retVal = true;
				} catch (Exception e) {
					// retVal = STR_UNKNOWN;
				}
				return retVal;
		}
		
	    public  String getBluetoothKey(Context ctx) {
			String retVal = null;
			try {
				retVal = getSharedPreference(ctx).getString(BLUETOOTH_KEY, null);
				// retVal = readTextFromFile(FILE_NAME_ASSET);
			} catch (Exception e) {
			}
			return retVal;
		}
		public  boolean setDeviceIdKey(String prefVal,Context ctx) {
			boolean retVal = false;
				try {
						Editor editor = getSharedPreference(ctx).edit();
						editor.putString(DEVICE_ID, prefVal);
						editor.commit();
						retVal = true;
				} catch (Exception e) {
					// retVal = STR_UNKNOWN;
				}
				return retVal;
		}
		
	    public  String getCarRegNoKey(Context ctx) {
			String retVal = null;
			try {
				retVal = getSharedPreference(ctx).getString(CAR_REG_NO, null);
				// retVal = readTextFromFile(FILE_NAME_ASSET);
			} catch (Exception e) {
			}
			return retVal;
		}
		public  boolean setCarRegNoKey(String prefVal,Context ctx) {
			boolean retVal = false;
				try {
						Editor editor = getSharedPreference(ctx).edit();
						editor.putString(CAR_REG_NO, prefVal);
						editor.commit();
						retVal = true;
				} catch (Exception e) {
					// retVal = STR_UNKNOWN;
				}
				return retVal;
		}

		   public  String getAssetIdKey(Context ctx) {
				String retVal = null;
				try {
					retVal = getSharedPreference(ctx).getString(ASSET_ID, null);
					// retVal = readTextFromFile(FILE_NAME_ASSET);
				} catch (Exception e) {
				}
				return retVal;
			}
			public  boolean setAssetIdKey(String prefVal,Context ctx) {
				boolean retVal = false;
					try {
							Editor editor = getSharedPreference(ctx).edit();
							editor.putString(ASSET_ID, prefVal);
							editor.commit();
							retVal = true;
					} catch (Exception e) {
						// retVal = STR_UNKNOWN;
					}
					return retVal;
			}
			
			   public  boolean getVideoRestart(Context ctx) {
					boolean retVal = false;
					try {
						retVal = getSharedPreference(ctx).getBoolean(VIDEO_RESTART, false);
						// retVal = readTextFromFile(FILE_NAME_ASSET);
					} catch (Exception e) {
					}
					return retVal;
				}
				public  boolean setVideoRestart(boolean prefVal,Context ctx) {
					boolean retVal = false;
						try {
								Editor editor = getSharedPreference(ctx).edit();
								editor.putBoolean(VIDEO_RESTART, prefVal);
								editor.commit();
								retVal = true;
						} catch (Exception e) {
							// retVal = STR_UNKNOWN;
						}
						return retVal;
				}
}
