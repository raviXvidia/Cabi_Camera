package com.xvidia.cabi.utils;


import com.xvidia.cabi.service.MyApplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;



public class AppUtil {

//       public static Bitmap ConvertToImage(String image){
//	    try{
//	    	String imageDataBytes = image.substring(image.indexOf(",")+1);
//
//	    	InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
//	        Bitmap bitmap = BitmapFactory.decodeStream(stream);
//	        return bitmap;  
//	    }
//	    catch (Exception e) {
//	        return null;            
//	    }
//	}
//       public static Bitmap RotateBitmap(Bitmap source, float angle)
//   	{
//   	      Matrix matrix = new Matrix();
//   	      matrix.postRotate(angle);
//   	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
//   	}
       
        
       public static boolean checkNetwrk(){
		boolean nwFlag = false;
		try{		
			ConnectivityManager connMgr = (ConnectivityManager) MyApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				nwFlag = true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return nwFlag;
	}
       /**
   	 * This function hides the soft Keyboard
   	 * @param editbox -reference to editext for which soft keyboard is to be hidden
   	 * @since version 1.0
   	 */
   	public static void hideSoftKeyboard(Context ctx, EditText editbox){
   		InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
   		imm.hideSoftInputFromWindow(editbox.getWindowToken(), 0);
   	}   
	public static boolean verifyMobile(String mobileNo){
		boolean retFlag= false;
		if(!mobileNo.isEmpty()&& mobileNo.length() == 10){
			retFlag= true;
		}
		
		return retFlag;
	}
	public static boolean verifyEmail(String email){
		boolean retFlag= false;
		if(!email.isEmpty()&& Patterns.EMAIL_ADDRESS.matcher(email).matches()){
			retFlag= true;
		}
		
		return retFlag;
	}
}
