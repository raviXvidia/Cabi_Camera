package com.xvidia.cabi.network;


public interface IAPIConstants {
//	public final int API_KEY_REGISTRATION = 0;
//	public final int API_KEY_VIEW_USER_PROFILE = 1;
	int API_KEY_DEVICE_INIT =1;
	int API_KEY_REGISTER_DEVICE = 2;
	int API_KEY_UPLOAD_VIDEO = 3;
	int API_KEY_PRE_REG_DEMO = 4;
	int API_KEY_OBD_COOLANT_TEMPERATURE = 5;
	int API_KEY_OBD_SPEED =6;
	int API_KEY_OBD_PRESSURE = 7;
	int API_KEY_OBD_THROTTLE= 8;
	int API_KEY_OBD_RPM = 9;
	int API_KEY_OBD_FUELTYPE= 10;
	int API_KEY_LOCATION = 11;
	int API_KEY_GET_PUBLISHER = 12;
	int API_KEY_GET_SUBSCRIBER = 13;
	int API_KEY_LIVE_CLOSE= 14;
	int API_KEY_UPDATE_LIVE_API_KEY = 15;
	
   
	
}
