package com.xvidia.cabi.network.model;

/**
 * Created by vasu on 21/12/15.
 */
public class ChatMessage {

    private String messageText;
    private String ownerThumbNail;
    private String ownerDisplayName;
    private String owner;
    private Boolean mRemote;



    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getOwnerThumbNail() {
        return ownerThumbNail;
    }

    public void setOwnerThumbNail(String ownerThumbNail) {
        this.ownerThumbNail = ownerThumbNail;
    }

    public String getOwnerDisplayName() {
        return ownerDisplayName;
    }

    public void setOwnerDisplayName(String ownerDisplayName) {
        this.ownerDisplayName = ownerDisplayName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Boolean getmRemote() {
        return mRemote;
    }

    public void setmRemote(Boolean mRemote) {
        this.mRemote = mRemote;
    }


    @Override
    public String toString() {
        return "ChatMessage{" +
                "messageText='" + messageText + '\'' +
                ", ownerThumbNail='" + ownerThumbNail + '\'' +
                ", ownerDisplayName='" + ownerDisplayName + '\'' +
                ", owner='" + owner + '\'' +
                ", mRemote=" + mRemote +
                '}';
    }
}
